/*
This RFduino sketch demonstrates a full bi-directional Bluetooth Low
Energy 4 connection between an iPhone application and an RFduino.

This sketch works with the rfduinoLedButton iPhone application.

The button on the iPhone can be used to turn the green led on or off.
The button state of button 1 is transmitted to the iPhone and shown in
the application.
*/

/*
 Copyright (c) 2014 OpenSourceRF.com.  All right reserved.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <RFduinoBLE.h>

// pin 2 on the RGB shield is the red led
int led1 = 2;
// pin 3 on the RGB shield is the green led
int vibrationMotor = 3;
// pin 4 on the RGB shield is the blue led
//int led3 = 4;

// pin 5 on the RGB shield is button 1
// (button press will be shown on the iPhone app)
//int button = 5;

// debounce time (in ms)
//int debounce_time = 10;

// maximum debounce timeout (in ms)
//int debounce_timeout = 100;

void setup() {
  // led turned on/off from the iPhone app
  pinMode(led1, OUTPUT);

  // button press will be shown on the iPhone app)
//  pinMode(button, INPUT);
  
  Serial.begin(9600);
  Serial.println("Starting up..");

  // this is the data we want to appear in the advertisement
  // (if the deviceName and advertisementData are too long to fix into the 31 byte
  // ble advertisement packet, then the advertisementData is truncated first down to
  // a single byte, then it will truncate the deviceName)
  RFduinoBLE.deviceName = "Dinovis";
  RFduinoBLE.advertisementData = "Dinovis Bracelet";
  
  // start the BLE stack
  RFduinoBLE.begin();
}

void loop() {

}

void RFduinoBLE_onDisconnect()
{
  Serial.println("BLE disconnected");
  // don't leave the led on if they disconnect
  digitalWrite(led1, LOW);
}

void RFduinoBLE_onReceive(char *data, int len) {
  
  uint8_t repetitionMode = data[0];
  uint8_t numberOfRepetitions = data[1];
  
  int pairs = (len - 2) / 2;
  
  uint8_t intensity;
  uint8_t duration;

  printDebugInformation(data, len, pairs, repetitionMode, numberOfRepetitions);
  
  if (numberOfRepetitions < 6) {
    
    // are we repeating all or only parts?
    boolean repeatAll;
    if (repetitionMode == 0 || repetitionMode >= pairs) {
      repeatAll = true;
      Serial.print("* Repeating ALL ");
      Serial.print(pairs);
      Serial.println(" entries *");
    } else {
      repeatAll = false;
      Serial.print("* Repeating ");
      Serial.print(repetitionMode);
      Serial.print(" out of ");
      Serial.print(pairs);
      Serial.println(" total entries *");
    }
    
    /* REPEATING ALL CODE */
    if (repeatAll) {
      for (int j = 0; j < numberOfRepetitions; j++) {
        
        Serial.print("### REPETITION ");
        Serial.print(j+1);
        Serial.print(" of ");
        Serial.print(numberOfRepetitions);
        Serial.println(" ###");
      
        for (int i = 2; i < len; i++) {
          intensity = data[i];
          duration = data[++i];
      
          setIntensityForDuration(intensity, duration);
        }
      }  
    } 
    
    /* PARTIAL REPEAT CODE */ 
    else {
      
      int i;
      
      for (int j = 0; j < numberOfRepetitions; j++) {
        i = 2;
        
        Serial.print("### REPETITION ");
        Serial.print(j+1);
        Serial.print(" of ");
        Serial.print(numberOfRepetitions);
        Serial.println(" ###");
      
        while (i < (repetitionMode * 2) + 2) {
          intensity = data[i];
          duration = data[++i];
          i++;
      
          setIntensityForDuration(intensity, duration);
        }
      }      
      
      int remainingEntryPairs = (len - i) / 2;
      Serial.println("### REPETITIONS DONE ###");
      Serial.print("Executing ");
      Serial.print(remainingEntryPairs);
      if (remainingEntryPairs > 1) {
        Serial.println(" remaining entries"); 
      } else {
        Serial.println(" remaining entry"); 
      }
      while(i < len) {
        intensity = data[i];
        duration = data[++i];
        i++;
    
        setIntensityForDuration(intensity, duration);
      }
      
    }
    
    /* DONE WITH PATTERNS */
    setIntensityZero(); 
    
  } else {
    Serial.print("MAXIMUM NUMBER OF REPETITIONS EXEED 5! ");
    Serial.print(numberOfRepetitions);
    Serial.println(" is larger than 5!");
  }
  Serial.println("-");
}

void printDebugInformation(char *data, int len, int pairs, uint8_t repetitionMode, uint8_t numberOfRepetitions) {
  
  Serial.print("Data with len ");
  Serial.print(len);
  Serial.println(" just came in");

  Serial.print("Data: ");
  for (int i = 0; i < len; i++) {
     uint8_t x = data[i];
     Serial.print(x);
     Serial.print(" ");
  }
  Serial.println("");

  Serial.print("Repetition mode: ");
  Serial.print(repetitionMode);
  Serial.print(", repetitions: ");
  Serial.println(numberOfRepetitions);
  
  Serial.print("Intensity/duration pairs: ");
  Serial.println(pairs);
  
}

void setIntensityForDuration(uint8_t intensity, uint8_t duration) {
  
  int dur = duration * 10;
  
  Serial.print("Intensity of ");
  Serial.print(intensity);
  Serial.print(" for ");
  Serial.print(dur);
  Serial.print("ms.. ");
  
  setIntensity(intensity);
  delay(dur);
  Serial.println("done");
}

void setIntensity(uint8_t intensity) {
  // set PWM for each led
  //analogWrite(led1, intensity);
  analogWrite(vibrationMotor, intensity);
  //analogWrite(led3, intensity);
}

void setIntensityZero() {
  setIntensity(0);
}
