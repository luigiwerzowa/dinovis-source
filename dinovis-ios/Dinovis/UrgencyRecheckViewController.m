//
//  UrgencyRecheckController.m
//  Dinovis
//
//  Created by Ludwig Werzowa on 23.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import "UrgencyRecheckViewController.h"

#import "ActivityViewController.h"

@implementation UrgencyRecheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationItem *navItem = [self navigationItem];
    [navItem setTitle:@"Urgency Recheck"];
    
    [self.lowUrgencyButtonOutlet setTitle:[self getLowUrgencyLabel] forState:UIControlStateNormal];
    
    [self.mediumUrgencyButtonOutlet setTitle:[self getMediumUrgencyLabel] forState:UIControlStateNormal];
    
    [self.highUrgencyButtonOutlet setTitle:[self getHighUrgencyLabel] forState:UIControlStateNormal];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.validityCheckLabel.text = [NSString stringWithFormat:@"Order: %@", [self getUrgencySignalOrder]];
}


# pragma mark - Navigation controls

- (void)nextView {
    
    if ([self.urgencyCheckSwitchOutlet isOn]) {
        
        ALog(@"Next view called (ActivityViewController)");
        
        UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ActivityViewController *viewController = [storybord instantiateViewControllerWithIdentifier:@"ActivityViewController"];
        
        viewController.rfduino = self.rfduino;
        
        [[self navigationController] pushViewController:viewController animated:YES];
    } else {
        
        ALog(@"Next view called (ActivityViewController) - IMPOSSIBLE");
        
        self.validityCheckLabel.text = @"Next view not yet available!";
    }
}

#pragma mark - Button handlers

- (IBAction)lowUrgencyButton:(id)sender {
    ALog(@"Send low urgency button up");
    
    [self sendUserSpecificLowUrgencySignal];
}

- (IBAction)mediumUrgencyButton:(id)sender {
    ALog(@"Send medium urgency button up");
    
    [self sendUserSpecificMediumUrgencySignal];
}

- (IBAction)highUrgencyButton:(id)sender {
    ALog(@"Send high urgency button up");
    
    [self sendUserSpecificHighUrgencySignal];
}

@end
