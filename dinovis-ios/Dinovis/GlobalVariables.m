//
//  GlobalVariables.m
//  Dinovis
//
//  Created by Ludwig Werzowa on 19.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import "GlobalVariables.h"

@implementation GlobalVariables

@synthesize signalAnotC = _signalAnotC;
@synthesize signalCnotB = _signalCnotB;
@synthesize signalBnotA = _signalBnotA;

+ (GlobalVariables *)sharedInstance {
    static dispatch_once_t onceToken;
    static GlobalVariables *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[GlobalVariables alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        _signalAnotC = 0;
        _signalCnotB = 0;
        _signalBnotA = 0;
        _signalOrder = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSInteger)getSignalAorC {
    
    ALog(@"getSignalAorC called: %c", _signalAnotC);
    
    switch (_signalAnotC) {
        case 'A':
            return 0;
            break;
            
        case 'C':
            return 1;
            break;
            
        default:
            return UISegmentedControlNoSegment;
            break;
    }
    
}

- (NSInteger)getSignalCorB {
    
    ALog(@"getSignalCorB called: %c", _signalCnotB);
    
    switch (_signalCnotB) {
        case 'C':
            return 0;
            break;
            
        case 'B':
            return 1;
            break;
            
        default:
            return UISegmentedControlNoSegment;
            break;
    }
    
}

- (NSInteger)getSignalBorA {
    
    ALog(@"getSignalBorA called: %c", _signalBnotA);
    
    switch (_signalBnotA) {
        case 'B':
            return 0;
            break;
            
        case 'A':
            return 1;
            break;
            
        default:
            return UISegmentedControlNoSegment;
            break;
    }
    
}

- (void) calculateOrder {
    
    int a = 0;
    int b = 0;
    int c = 0;
    
    if (_signalAnotC == 'A') a++;
    if (_signalAnotC == 'C') c++;
    
    if (_signalCnotB == 'C') c++;
    if (_signalCnotB == 'B') b++;
    
    if (_signalBnotA == 'B') b++;
    if (_signalBnotA == 'A') a++;
    
    
    if (a != b && a != c && b != c) {
        ALog(@"getOrder called: A = %tu, B = %tu, C = %tu; Status: OK", a, b, c);
        [_signalOrder removeAllObjects];
        if (a == 2) [_signalOrder addObject:@"A"];
        else if (b == 2) [_signalOrder addObject:@"B"];
        else if (c == 2) [_signalOrder addObject:@"C"];

        if (a == 1) [_signalOrder addObject:@"A"];
        else if (b == 1) [_signalOrder addObject:@"B"];
        else if (c == 1) [_signalOrder addObject:@"C"];
        
        if (a == 0) [_signalOrder addObject:@"A"];
        else if (b == 0) [_signalOrder addObject:@"B"];
        else if (c == 0) [_signalOrder addObject:@"C"];
    } else {
        ALog(@"getOrder called: A = %tu, B = %tu, C = %tu; Status: *NOT* OK", a, b, c);
        [_signalOrder removeAllObjects];
    }
}

- (BOOL) isValidSelection {
    if ([_signalOrder count] == 3) return YES;
    return NO;
}

@end
