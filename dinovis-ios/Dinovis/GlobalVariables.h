//
//  GlobalVariables.h
//  Dinovis
//
//  Created by Ludwig Werzowa on 19.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"

@interface GlobalVariables : NSObject {
    char _signalAnotC;
    char _signalCnotB;
    char _signalBnotA;
    NSMutableArray* _signalOrder;
}

+ (GlobalVariables *)sharedInstance;
- (NSInteger)getSignalAorC;
- (NSInteger)getSignalCorB;
- (NSInteger)getSignalBorA;

- (BOOL) isValidSelection;
- (void) calculateOrder;

@property(assign, nonatomic, readwrite) char signalAnotC;
@property(assign, nonatomic, readwrite) char signalCnotB;
@property(assign, nonatomic, readwrite) char signalBnotA;
@property(strong, nonatomic, readwrite) NSMutableArray *signalOrder;

@end
