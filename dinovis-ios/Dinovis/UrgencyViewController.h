//
//  UrgencyViewController.h
//  Dinovis
//
//  Created by Ludwig Werzowa on 13.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import "BaseViewController.h"

@interface UrgencyViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *signalAorCSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *signalCorBSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *signalBorASegment;

- (IBAction)signalAorCChanged:(id)sender;
- (IBAction)signalCorBChanged:(id)sender;
- (IBAction)signalBorAChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *validityCheckLabel;

@end
