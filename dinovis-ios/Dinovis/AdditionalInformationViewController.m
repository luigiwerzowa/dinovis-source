//
//  AdditionalInformationViewController.m
//  Dinovis
//
//  Created by Ludwig Werzowa on 19.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import "AdditionalInformationViewController.h"

@implementation AdditionalInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationItem *navItem = [self navigationItem];
    [navItem setTitle:@"Additional Info"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button handlers

- (IBAction)lowUrgencyAdditionalInfo4Button:(id)sender {
    
    // send user specific low urgency signal
    [self sendUserSpecificLowUrgencySignal];
    
    // now send the additional information
    [self sendAdditionalInformation:4];
}

- (IBAction)highUrgencyAdditionalInfo2Button:(id)sender {
    
    // send user specific low urgency signal
    [self sendUserSpecificHighUrgencySignal];
    
    // now send the additional information
    [self sendAdditionalInformation:2];
}

- (IBAction)mediumUrgencyAdditionalInfo9Button:(id)sender {
    
    // send user specific low urgency signal
    [self sendUserSpecificMediumUrgencySignal];
    
    // now send the additional information
    [self sendAdditionalInformation:9];

    
}
@end
