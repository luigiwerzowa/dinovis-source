//
//  BaseViewController.m
//  Dinovis
//
//  Created by Ludwig Werzowa on 13.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import "BaseViewController.h"
#import "GlobalVariables.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                target:self
                                                                                action:@selector(nextView)];
    [[self navigationItem] setRightBarButtonItem:doneButton];
    
    _globals = [GlobalVariables sharedInstance];
    
    _rfduino.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Navigation controls

- (void)nextView {
    ALog(@"Next view called (nothing will happen)");
}


#pragma mark - General purpose methods

- (NSString*) getUrgencySignalOrder {
    NSString* output = nil;
    if ([_globals isValidSelection]) {
        output = [NSString stringWithFormat:@"%@ > %@ > %@", [_globals.signalOrder objectAtIndex:0], [_globals.signalOrder objectAtIndex:1], [_globals.signalOrder objectAtIndex:2]];
    }
    return output;
}

/* 
 * Get the label for the low urgency signal. If the user chose a different signal than expected, it will be displayed.
 * Two possibilities:
 * "Low urgency (as expected)"
 * or
 * "Low urgency (really signal X)"
 */
- (NSString*) getLowUrgencyLabel {
    
    NSString* output = nil;
    if ([_globals isValidSelection]) {
        output = [NSString stringWithFormat:@"Low urgency (%@)", [[_globals.signalOrder objectAtIndex:2] isEqualToString:@"C"] ? @"as expected" : [NSString stringWithFormat:@"really signal %@", [_globals.signalOrder objectAtIndex:2]]];
    }
    return output;
}

/*
 * Get the label for the medium urgency signal. If the user chose a different signal than expected, it will be displayed.
 * Two possibilities:
 * "Medium urgency (as expected)"
 * or
 * "Medium urgency (really signal X)"
 */
- (NSString*) getMediumUrgencyLabel {
    
    NSString* output = nil;
    if ([_globals isValidSelection]) {
        output = [NSString stringWithFormat:@"Medium urgency (%@)", [[_globals.signalOrder objectAtIndex:1] isEqualToString:@"B"] ? @"as expected" : [NSString stringWithFormat:@"really signal %@", [_globals.signalOrder objectAtIndex:1]]];
    }
    return output;
}

/*
 * Get the label for the high urgency signal. If the user chose a different signal than expected, it will be displayed.
 * Two possibilities:
 * "High urgency (as expected)"
 * or
 * "High urgency (really signal X)"
 */
- (NSString*) getHighUrgencyLabel {
    
    NSString* output = nil;
    if ([_globals isValidSelection]) {
        output = [NSString stringWithFormat:@"High urgency (%@)", [[_globals.signalOrder objectAtIndex:0] isEqualToString:@"A"] ? @"as expected" : [NSString stringWithFormat:@"really signal %@", [_globals.signalOrder objectAtIndex:0]]];
    }
    return output;
}

#pragma mark - Sending messages - Interface methods

- (void) sendTestSignal {
    
    ALog(@"Sending test signal");
    
    NSInteger repeatMode   = [@"0" integerValue];
    NSInteger repeats      = [@"2" integerValue];
    NSInteger intensity1   = [@"255" integerValue];
    NSInteger duration1    = [@"100" integerValue];
    NSInteger intensity2   = [@"150" integerValue];
    NSInteger duration2    = [@"50" integerValue];
    
    uint8_t* signal = malloc(6*sizeof(uint8_t *));
    signal[0] = repeatMode;
    signal[1] = repeats;
    signal[2] = intensity1;
    signal[3] = duration1;
    signal[4] = intensity2;
    signal[5] = duration2;
    
    [self sendPattern:signal withNumberOfEntries:6];
    
    //    free(signal);
}

- (void) sendLowUrgencySignal {
    ALog(@"Sending low urgency signal");
    
    [self sendPattern:LOW_URGENCY_SIGNAL withNumberOfEntries:sizeof(LOW_URGENCY_SIGNAL)];
}

- (void) sendMediumUrgencySignal {
    ALog(@"Sending medium urgency signal");
    
    [self sendPattern:MEDIUM_URGENCY_SIGNAL withNumberOfEntries:sizeof(MEDIUM_URGENCY_SIGNAL)];
}

- (void) sendHighUrgencySignal {
    ALog(@"Sending high urgency signal");
    
    [self sendPattern:HIGH_URGENCY_SIGNAL_PART withNumberOfEntries:sizeof(HIGH_URGENCY_SIGNAL_PART)];
}

/*
 * Sends the signal the user perceives as least urgent
 */
- (void) sendUserSpecificLowUrgencySignal {
 
    // get the signal the user thinks has the lowest urgency
    NSString* lowUrgency = [_globals.signalOrder objectAtIndex:2];
    
    ALog(@"Low urgency button up. Activating signal %@", lowUrgency);
    
    // correct: lowest is lowest -> send lowest
    if ([lowUrgency  isEqual: @"C"]) {
        [self sendLowUrgencySignal];
    }
    
    // user thinks the second highest urgency is lowest -> send medium
    else if ([lowUrgency  isEqual: @"B"]) {
        [self sendMediumUrgencySignal];
    }
    
    // user thinks the highest urgency is lowest -> send highest
    else if ([lowUrgency  isEqual: @"A"]) {
        [self sendHighUrgencySignal];
    }

}

/*
 * Sends the signal the user perceives as medium urgent
 */
- (void) sendUserSpecificMediumUrgencySignal {
    
    // get the signal the user thinks has medium urgency
    NSString* mediumUrgency = [_globals.signalOrder objectAtIndex:1];
    
    ALog(@"Medium urgency button up. Activating signal %@", mediumUrgency);
    
    // correct: medium is medium -> send medium
    if ([mediumUrgency isEqual: @"B"]) {
        [self sendMediumUrgencySignal];
    }
    
    // user thinks the highest urgency is medium -> send highest
    else if ([mediumUrgency isEqual: @"A"]) {
        [self sendHighUrgencySignal];
    }
    
    // user thinks the lowest urgency is medium -> send lowest
    else if ([mediumUrgency  isEqual: @"C"]) {
        [self sendLowUrgencySignal];
    }

}

/*
 * Sends the signal the user perceives as most urgent
 */
- (void) sendUserSpecificHighUrgencySignal {
    
    // get the signal the user thinks has the highest urgency
    NSString* highUrgency = [_globals.signalOrder objectAtIndex:0];
    
    ALog(@"High urgency button up. Activating signal %@", highUrgency);
    
    // correct: highest is highest -> send highest
    if ([highUrgency isEqual: @"A"]) {
        [self sendHighUrgencySignal];
    }
    
    // user thinks the second highest urgency is highest -> send medium
    else if ([highUrgency isEqual: @"B"]) {
        [self sendMediumUrgencySignal];
    }
    
    // user thinks the lowest urgency is highest -> send lowest
    else if ([highUrgency  isEqual: @"C"]) {
        [self sendLowUrgencySignal];
    }

}

#pragma mark - Sending messages

/*
 * This method sends data to the RFduino.
 */
- (void)sendPattern:(const uint8_t[]) patternArray withNumberOfEntries:(int)numberOfEntries
{
    ALog(@"sendPattern. Number of entries: %d", numberOfEntries);
    
    for (int i = 0; i < numberOfEntries; i++) {
        
        ALog(@"patternArrayEntry[%d]: %hhu", i, patternArray[i]);
    }
    
    NSData *data = [NSData dataWithBytes:(void*)patternArray length:numberOfEntries];
    
    ALog(@"** data: %@", data);
    
    [_rfduino send:data];
    
}

/*
 * This method send additional information to the RFduino
 */
- (void)sendAdditionalInformation:(int)messageNumber {
    ALog(@"Sending additional information with number %d", messageNumber);
    
    [self sendPattern:ADDITIONAL_START withNumberOfEntries:sizeof(ADDITIONAL_START)];
    
    int i = messageNumber;
    
    while (i > 0) {
        
        switch (i) {
            case 1:
                [self sendPattern:ADDITIONAL_ONE withNumberOfEntries:sizeof(ADDITIONAL_ONE)];
                break;
            case 2:
                [self sendPattern:ADDITIONAL_TWO withNumberOfEntries:sizeof(ADDITIONAL_TWO)];
                break;
                
            default:
                [self sendPattern:ADDITIONAL_THREE withNumberOfEntries:sizeof(ADDITIONAL_THREE)];
                break;
        }
        
        i -= 3;
        
    }
}

#pragma mark NotificationDelegate

/*
 * The delegation handler for push notifications in AppDelegate calls this method when
 * a push notification arrives.
 */
-(void)processNotification:(NSDictionary *)userInfo
{
    ALog(@"processNotification");
    
    //NSString* alertValue = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    
    int numberOfEntries = 0;
    
    _messagesTextView.text = userInfo.description;
    
    if ([userInfo valueForKey:@"intensity4"] != nil) {
        numberOfEntries = 10;
        ALog(@"FOUR elements, number of entries: %d", numberOfEntries);
    } else if ([userInfo valueForKey:@"intensity3"] != nil) {
        numberOfEntries = 8;
        ALog(@"THREE elements, number of entries: %d", numberOfEntries);
    } else if ([userInfo valueForKey:@"intensity2"] != nil) {
        numberOfEntries = 6;
        ALog(@"TWO elements, number of entries: %d", numberOfEntries);
    } else if ([userInfo valueForKey:@"intensity1"] != nil) {
        numberOfEntries = 4;
        ALog(@"ONE element, number of entries: %d", numberOfEntries);
    } else {
        ALog(@"** NO ELEMENT **");
        return;
    }
    
    uint8_t* signal = malloc(numberOfEntries*sizeof(uint8_t *));
    
    switch (numberOfEntries) {
        case 10:
            signal[9] = [[userInfo valueForKey:@"duration4"] integerValue];
            signal[8] = [[userInfo valueForKey:@"intensity4"] integerValue];
        case 8:
            signal[7] = [[userInfo valueForKey:@"duration3"] integerValue];
            signal[6] = [[userInfo valueForKey:@"intensity3"] integerValue];
        case 6:
            signal[5] = [[userInfo valueForKey:@"duration2"] integerValue];
            signal[4] = [[userInfo valueForKey:@"intensity2"] integerValue];
        case 4:
            signal[3] = [[userInfo valueForKey:@"duration1"] integerValue];
            signal[2] = [[userInfo valueForKey:@"intensity1"] integerValue];
            signal[1] = [[userInfo valueForKey:@"repeats"] integerValue];;
            signal[0] = [[userInfo valueForKey:@"repeatMode"] integerValue];;
            break;
            
        default:
            ALog(@"** Default case - something went wrong! **");
            break;
    }
    
    //    [self printSignalArray:signal withSize:numberOfEntries];
    
    [self sendPattern:signal withNumberOfEntries:numberOfEntries];
    
    free(signal);
}

// TODO remove
-(void)printSignalArray:(const uint8_t[])arr withSize:(int)size {
    for(int i = 0; i < size; i++) {
        NSLog(@"signal[%d]: %hhu", i, arr[i]);
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
