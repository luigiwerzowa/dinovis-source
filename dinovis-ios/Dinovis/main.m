//
//  main.m
//  Dinovis
//
//  Created by Ludwig Werzowa on 07.10.14.
//  Copyright (c) 2014 Ludwig Werzowa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
