//
//  ActivityViewController.h
//  Dinovis
//
//  Created by Ludwig Werzowa on 19.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import "BaseViewController.h"

@interface ActivityViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIButton *lowUrgencyButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *highUrgencyButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *mediumUrgencyButtonOutlet;

@property (weak, nonatomic) IBOutlet UIButton *lowUrgencyButtonOutlet2;
@property (weak, nonatomic) IBOutlet UIButton *highUrgencyButtonOutlet2;
@property (weak, nonatomic) IBOutlet UIButton *mediumUrgencyButtonOutlet2;

@end
