//
//  ActivityViewController.m
//  Dinovis
//
//  Created by Ludwig Werzowa on 19.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import "ActivityViewController.h"

#import "AdditionalInformationViewController.h"

@implementation ActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationItem *navItem = [self navigationItem];
    [navItem setTitle:@"Activity Test"];
    
    [self.lowUrgencyButtonOutlet setTitle:[self getLowUrgencyLabel] forState:UIControlStateNormal];
    
    [self.mediumUrgencyButtonOutlet setTitle:[self getMediumUrgencyLabel] forState:UIControlStateNormal];
    
    [self.highUrgencyButtonOutlet setTitle:[self getHighUrgencyLabel] forState:UIControlStateNormal];
    
    [self.lowUrgencyButtonOutlet2 setTitle:[self getLowUrgencyLabel] forState:UIControlStateNormal];
    
    [self.mediumUrgencyButtonOutlet2 setTitle:[self getMediumUrgencyLabel] forState:UIControlStateNormal];
    
    [self.highUrgencyButtonOutlet2 setTitle:[self getHighUrgencyLabel] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Navigation controls

- (void)nextView {
    
    ALog(@"Next view called (AdditionalInformationViewController)");
    
    UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AdditionalInformationViewController *viewController = [storybord instantiateViewControllerWithIdentifier:@"AdditionalInformationViewController"];
        
    viewController.rfduino = self.rfduino;
    
    [[self navigationController] pushViewController:viewController animated:YES];

}

#pragma mark - Button handlers

- (IBAction)lowUrgencyButton:(id)sender {
    
    [self sendUserSpecificLowUrgencySignal];
}

- (IBAction)highUrgencyButton:(id)sender {
    
    [self sendUserSpecificHighUrgencySignal];
}

- (IBAction)mediumUrgencyButton:(id)sender {
    
    [self sendUserSpecificMediumUrgencySignal];
}

@end
