//
//  TestSignalController.m
//  Dinovis
//
//  Created by Ludwig Werzowa on 07.10.14.
//  Copyright (c) 2014 Ludwig Werzowa. All rights reserved.
//

#import "TestSignalViewController.h"

#import "UrgencyViewController.h"

@interface TestSignalViewController ()

@end

@implementation TestSignalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationItem *navItem = [self navigationItem];
    [navItem setTitle:@"Test Signal"];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    //Dispose of any resources that can be recreated.
}

# pragma mark - Navigation controls

- (void)nextView {
    ALog(@"Next view called (UrgencyViewController)");
    
    UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UrgencyViewController *viewController = [storybord instantiateViewControllerWithIdentifier:@"UrgencyViewController"];
    
    viewController.rfduino = self.rfduino;
    
    [[self navigationController] pushViewController:viewController animated:YES];
}

# pragma mark - Button actions

/*
 * This is the test signal button that just sends a default signal
 */
- (IBAction)testSignalButton:(id)sender {
    [self sendTestSignal];
}

- (IBAction)sendSignalA:(id)sender {
    [self sendHighUrgencySignal];
}

- (IBAction)sendSignalB:(id)sender {
    [self sendMediumUrgencySignal];
}

- (IBAction)sendSignalC:(id)sender {
    [self sendLowUrgencySignal];
}

@end
