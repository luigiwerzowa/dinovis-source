//
//  UrgencyViewController.m
//  Dinovis
//
//  Created by Ludwig Werzowa on 13.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import "UrgencyViewController.h"
#import "GlobalVariables.h"

#import "UrgencyRecheckViewController.h"

@interface UrgencyViewController ()

@end

@implementation UrgencyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationItem *navItem = [self navigationItem];
    [navItem setTitle:@"Urgency Test"];
    
    /*
     * Sets the correct state
     */
    ALog(@"Setting segmented controls");
    self.signalAorCSegment.selectedSegmentIndex = [self.globals getSignalAorC];
    self.signalCorBSegment.selectedSegmentIndex = [self.globals getSignalCorB];
    self.signalBorASegment.selectedSegmentIndex = [self.globals getSignalBorA];
    
    [self doValidityCheck];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Navigation controls

- (void)nextView {
    
    if ([self.globals isValidSelection]) {
        
        ALog(@"Next view called (UrgencyRecheckViewController)");
    
        UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UrgencyRecheckViewController *viewController = [storybord instantiateViewControllerWithIdentifier:@"UrgencyRecheckViewController"];
    
        viewController.rfduino = self.rfduino;
    
        [[self navigationController] pushViewController:viewController animated:YES];
    } else {
        
        ALog(@"Next view called (UrgencyRecheckViewController) - IMPOSSIBLE");
        
        self.validityCheckLabel.text = @"Next view not yet available!";
    }
}

# pragma mark - Button actions

- (IBAction)highUrgencyButton:(id)sender {
    ALog(@"High urgency button up");
    
    [self sendHighUrgencySignal];
}

- (IBAction)mediumUrgencyButton:(id)sender {
    ALog(@"Medium urgency button up");
    
    [self sendMediumUrgencySignal];
}

- (IBAction)lowUrgencyButton:(id)sender {
    ALog(@"Low urgency button up");
    
    [self sendLowUrgencySignal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Segmented Control

- (IBAction)signalAorCChanged:(id)sender {
    
    char AorC = [sender selectedSegmentIndex] == 0 ? 'A' : 'C';
    
    ALog(@"Segmented control for A or C changed: %c", AorC);
    
    self.globals.signalAnotC = AorC;
    
    [self.globals calculateOrder];
    [self doValidityCheck];
}

- (IBAction)signalCorBChanged:(id)sender {
    
    char CorB = [sender selectedSegmentIndex] == 0 ? 'C' : 'B';
    
    ALog(@"Segmented control for C or B changed: %c", CorB);
    
    self.globals.signalCnotB = CorB;
    
    [self.globals calculateOrder];
    [self doValidityCheck];
}

- (IBAction)signalBorAChanged:(id)sender {
    
    char BorA = [sender selectedSegmentIndex] == 0 ? 'B' : 'A';
    
    ALog(@"Segmented control for B or A changed: %c", BorA);
    
    self.globals.signalBnotA = BorA;
    
    [self.globals calculateOrder];
    [self doValidityCheck];
}

- (void) doValidityCheck {
    
    if ([self.globals isValidSelection]) {
        NSString* output = [NSString stringWithFormat:@"Selection is valid. Order: %@", [self getUrgencySignalOrder]];
        self.validityCheckLabel.text = output;
    } else {
        self.validityCheckLabel.text = @"Selection is invalid!";
    }
}

@end
