//
//  BaseViewController.h
//  Dinovis
//
//  Created by Ludwig Werzowa on 13.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RFduinoDelegate.h"
#import "RFduino.h"

@class GlobalVariables;

@protocol NotificationDelegate <NSObject>

-(void)processNotification:(NSDictionary *)userInfo;
- (void)nextView;

@end

@interface BaseViewController : UIViewController<RFduinoDelegate, NotificationDelegate>

/*
 * This type regulates the intensity of the signal.
 * It can have values ranging from 0 to 255.
 */
typedef NS_ENUM(uint8_t, IntensityType) {
    IntensityNull = 100,
    IntensityLow = 100,
    IntensityMedium = 200,
    IntensityMax = 255
};

/*
 * This type regulates the duration of a signal.
 * Due to limitations to uint8_t (which has a maximum
 * value of 255), the duration in milliseconds is stored
 * with a factor 10. This means that one second is
 * represented here as 100 instead of 1000.
 *
 * Values may range from 0 to 250 (2.5 seconds).
 */
typedef NS_ENUM(uint8_t, DurationType) {
    DurationShort = 25,
    DurationMedium = 60,
    DurationLong = 120
};

/*
 * This type regulates repeats of the pattern or
 * part of the pattern.
 */
typedef NS_ENUM(uint8_t, RepeatType) {
    RepeatAll,
    RepeatFirst,
    RepeatFirstTwo,
    RepeatFirstThree
};

typedef uint8_t array_of_2_uint8_t[2];

#define STANDARD_SIGNAL ((array_of_2_uint8_t[]) {{RepeatAll, 0}, {IntensityMedium, DurationMedium}, {IntensityNull, DurationShort}, {IntensityMedium, DurationMedium}, {IntensityNull, DurationShort}, {IntensityMax, DurationLong}})

#define RAISING_SIGNAL ((array_of_2_uint8_t[]) {{RepeatAll, 2}, {IntensityNull, DurationShort}, {IntensityLow, DurationShort}, {IntensityMedium, DurationShort}, {IntensityMax, DurationMedium}})

#define HIGH_URGENCY_SIGNAL_PART ((uint8_t[]) {RepeatAll, 5, IntensityMax, DurationShort, IntensityNull, DurationShort, IntensityMax, DurationShort, IntensityNull, DurationShort})

#define MEDIUM_URGENCY_SIGNAL ((uint8_t[]) {RepeatAll, 3, IntensityMax, DurationShort, IntensityNull, DurationShort, IntensityMax, DurationMedium, IntensityNull, DurationMedium})

#define LOW_URGENCY_SIGNAL ((uint8_t[]) {RepeatAll, 4, IntensityMax, DurationMedium, IntensityNull, DurationMedium})

#define ADDITIONAL_START ((uint8_t[]) {RepeatAll, 1, IntensityNull, DurationLong, IntensityMax, DurationLong, IntensityNull, DurationShort})
#define ADDITIONAL_ONE ((uint8_t[]) {RepeatFirstTwo, 1, IntensityNull, DurationShort, IntensityMax, DurationShort, IntensityNull, DurationShort})
#define ADDITIONAL_TWO ((uint8_t[]) {RepeatFirstTwo, 2, IntensityNull, DurationShort, IntensityMax, DurationShort, IntensityNull, DurationShort})
#define ADDITIONAL_THREE ((uint8_t[]) {RepeatFirstTwo, 3, IntensityNull, DurationShort, IntensityMax, DurationShort, IntensityNull, DurationShort})

@property(nonatomic, strong) RFduino *rfduino;
@property(nonatomic, strong) IBOutlet UITextView *messagesTextView;
@property(nonatomic, strong) GlobalVariables* globals;

//- (void)sendPattern:(const uint8_t[]) patternArray withNumberOfEntries:(int)numberOfEntries;
- (void)sendAdditionalInformation:(int)messageNumber;
- (NSString*) getUrgencySignalOrder;
- (NSString*) getLowUrgencyLabel;
- (NSString*) getMediumUrgencyLabel;
- (NSString*) getHighUrgencyLabel;

- (void) sendTestSignal;
- (void) sendLowUrgencySignal;
- (void) sendMediumUrgencySignal;
- (void) sendHighUrgencySignal;

- (void) sendUserSpecificLowUrgencySignal;
- (void) sendUserSpecificMediumUrgencySignal;
- (void) sendUserSpecificHighUrgencySignal;

@end
