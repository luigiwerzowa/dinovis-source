//
//  UrgencyRecheckController.h
//  Dinovis
//
//  Created by Ludwig Werzowa on 23.01.15.
//  Copyright (c) 2015 Ludwig Werzowa. All rights reserved.
//

#import "BaseViewController.h"

@interface UrgencyRecheckViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *validityCheckLabel;
@property (weak, nonatomic) IBOutlet UISwitch *urgencyCheckSwitchOutlet;

@property (weak, nonatomic) IBOutlet UIButton *lowUrgencyButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *mediumUrgencyButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *highUrgencyButtonOutlet;

@end
