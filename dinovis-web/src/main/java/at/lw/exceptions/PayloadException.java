package at.lw.exceptions;

/**
 * Created by Ludwig Werzowa on 14.04.16.
 */
public class PayloadException extends Exception {
    public PayloadException() {
        super();
    }

    public PayloadException(String message) {
        super(message);
    }

    public PayloadException(String message, Throwable cause) {
        super(message, cause);
    }

    public PayloadException(Throwable cause) {
        super(cause);
    }
}
