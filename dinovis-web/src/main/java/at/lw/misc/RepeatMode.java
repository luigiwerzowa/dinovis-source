/**
 * 
 */
package at.lw.misc;

/**
 * @author Ludwig Werzowa
 *
 */
public enum RepeatMode {

	REPEAT_ALL("Repeat all entries", 0), 
	REPEAT_FIRST("Repeat first entry", 1),
	REPEAT_SECOND("Repeat all up to second entry", 2),
	REPEAT_THIRD("Repeat all up to third entry", 3);
	
	private String text;
	private int code;
	
	RepeatMode(String text, int code) {
		this.text = text;
		this.code = code;
	}
	
	@Override
    public String toString() {
        return text;
    }
	
	public int getCode() {
		return code;
	}
}
