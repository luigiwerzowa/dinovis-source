package at.lw.misc;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Properties;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesHelper {

	private static final Logger logger = LoggerFactory
			.getLogger(PropertiesHelper.class);

	private static PropertiesHelper instance = null;

	private Properties props = new Properties();

	@SuppressWarnings("resource")
	private PropertiesHelper() {
		
		InputStream stream = null;
		Scanner scanner = null;

		try {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			stream = loader
					.getResourceAsStream("/settings.properties");
			scanner = new Scanner(stream).useDelimiter("\\A");
			String propertyFileContents = scanner.hasNext() ? scanner.next()
					: "";
			props.load(new StringReader(propertyFileContents.replace("\\",
					"\\\\")));

		} catch (IOException e) {
			throw new RuntimeException(
					"ERROR: settings.properties could not be loaded!");
		} finally {
			scanner.close();
			try {
				stream.close();
			} catch (IOException e) {
				logger.error("Unable to close InputStream", e);
			}
		}

	}

	public static String getProperty(String name) {
		if (instance == null) {
			instance = new PropertiesHelper();
		}

		return instance.props.getProperty(name);
	}

}
