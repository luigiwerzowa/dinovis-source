/**
 * 
 */
package at.lw.beans;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.lw.misc.PropertiesHelper;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;

/**
 * @author Ludwig Werzowa
 *
 */
@Startup
@Singleton
public class APNManagerSingleton {

	private final static Logger logger = LoggerFactory.getLogger(APNManagerSingleton.class);
	
	private static int counter = 0;
	
	private static final String ALERT_MESSAGE = "Vibration alarm #XXX has arrived";
	
	private ApnsService service;
	private String token;

	@PostConstruct
	private void startup() {

		logger.debug("Startup of APNManagerSingleton..");

		service = APNS.newService().withCert(PropertiesHelper.getProperty("key.location"), PropertiesHelper.getProperty("key.password")).withSandboxDestination().build();
		token = PropertiesHelper.getProperty("apns.token");
		
		logger.debug("Startup of APNManagerSingleton done!");

	}
	
	/**
	 * Sends a test notification
	 */
	public void sendTestNotification() {

		logger.debug("sendTestNotification..");

		String payload = APNS.newPayload().alertBody("Can't be simpler than this!").build();
		service.push(token, payload);

		logger.debug("sendTestNotification done!");
	}

	/**
	 * Constructs a push notification with a defined payload and sends it.
	 *
	 * @param repeatMode value of the custom payload field "repeatMode"
	 * @param repeats value of the custom payload field "repeats"
	 * @param values values for the custom payload fields according to the given map
	 */
	public void sendNotification(int repeatMode, int repeats, Map<String, String> values) {
		
		logger.debug("sendNotification. Repeat mode: " + repeatMode + ", Repeats: "+repeats+", values: "+values);
		
		String payload = APNS.newPayload().alertBody(getAlarmString()).customField("repeatMode", String.valueOf(repeatMode)).customField("repeats", String.valueOf(repeats)).customFields(values).build();
		service.push(token, payload);
		
		logger.debug("sendNotification done!");
	}
	
	private String getAlarmString() {
		return ALERT_MESSAGE.replaceFirst("XXX", String.valueOf(++counter));
	}

	@PreDestroy
	private void shutdown() {
		logger.debug("Shutting down ..");
		logger.debug("Done shutting down !");
	}

}
