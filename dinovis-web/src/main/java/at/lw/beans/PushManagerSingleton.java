///**
// * 
// */
//package at.lw.beans;
//
//import java.io.IOException;
//import java.security.KeyManagementException;
//import java.security.KeyStoreException;
//import java.security.NoSuchAlgorithmException;
//import java.security.UnrecoverableKeyException;
//import java.security.cert.CertificateException;
//import java.util.Collection;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import javax.ejb.Singleton;
//import javax.ejb.Startup;
//import javax.net.ssl.SSLHandshakeException;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import at.lw.misc.PropertiesHelper;
//
//import com.relayrides.pushy.apns.ApnsEnvironment;
//import com.relayrides.pushy.apns.ExpiredToken;
//import com.relayrides.pushy.apns.ExpiredTokenListener;
//import com.relayrides.pushy.apns.FailedConnectionListener;
//import com.relayrides.pushy.apns.PushManager;
//import com.relayrides.pushy.apns.PushManagerConfiguration;
//import com.relayrides.pushy.apns.RejectedNotificationListener;
//import com.relayrides.pushy.apns.RejectedNotificationReason;
//import com.relayrides.pushy.apns.util.ApnsPayloadBuilder;
//import com.relayrides.pushy.apns.util.MalformedTokenStringException;
//import com.relayrides.pushy.apns.util.SSLContextUtil;
//import com.relayrides.pushy.apns.util.SimpleApnsPushNotification;
//import com.relayrides.pushy.apns.util.TokenUtil;
//
///**
// * @author Ludwig Werzowa
// *
// */
//@Startup
//@Singleton
//public class PushManagerSingleton {
//
//	private final static Logger logger = LoggerFactory.getLogger(PushManagerSingleton.class);
//
//	private PushManager<SimpleApnsPushNotification> pushManager;
//
//	private byte[] token;
//	
//	@PostConstruct
//	private void startup() {
//
//		logger.debug("Startup of PushManagerSingleton..");
//		
//		try {
//			token = TokenUtil.tokenStringToByteArray(PropertiesHelper.getProperty("apns.token"));
//		} catch (MalformedTokenStringException e) {
//			logger.error("Unable to build token", e);
//		}
//		
//		try {
//			pushManager = new PushManager<SimpleApnsPushNotification>(ApnsEnvironment.getSandboxEnvironment(), SSLContextUtil.createDefaultSSLContext(
//					PropertiesHelper.getProperty("key.location"), PropertiesHelper.getProperty("key.password")), null, null, null,
//					new PushManagerConfiguration(), "ExamplePushManager");
//			
//			pushManager.start();
//			
//			pushManager.registerRejectedNotificationListener(new MyRejectedNotificationListener());
//			pushManager.registerFailedConnectionListener(new MyFailedConnectionListener());
//			pushManager.registerExpiredTokenListener(new MyExpiredTokenListener());
//			pushManager.requestExpiredTokens();
//			
//		} catch (UnrecoverableKeyException | KeyManagementException | KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
//			
//			logger.error("Unable to instanciate PushManager!", e);
//		}
//		
//		logger.debug("Startup of PushManagerSingleton done!");
//		
//	}
//
//	/**
//	 * Sends a test notification
//	 */
//	public void sendTestNotification() {
//
//		logger.debug("sendTestNotification..");
//
//		final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
//
//		payloadBuilder.setAlertBody("Ring ring, Neo.");
////		payloadBuilder.setSoundFileName("ring-ring.aiff");
//
//		final String payload = payloadBuilder.buildWithDefaultMaximumLength();
//
//		try {
//			pushManager.getQueue().put(new SimpleApnsPushNotification(token, payload));
//			logQueueStatus();
//		} catch (InterruptedException e) {
//			logger.error("Unable to send Notification", e);
//		}
//
//		logger.debug("sendTestNotification done!");
//	}
//	
//	private void logQueueStatus() {
//		logger.debug("Queue size: "+pushManager.getQueue().size()+"; Peek: "+pushManager.getQueue().peek());
//	}
//
//	@PreDestroy
//	private void shutdownPushManager() {
//		logger.debug("Shutting down PushManager..");
//		try {
//			logQueueStatus();
//			pushManager.shutdown();
//		} catch (InterruptedException e) {
//			logger.error("Unable to shutdown PushManager", e);
//		}
//		logger.debug("Done shutting down PushManager!");
//	}
//
//	private class MyExpiredTokenListener implements ExpiredTokenListener<SimpleApnsPushNotification> {
//
//		@Override
//		public void handleExpiredTokens(final PushManager<? extends SimpleApnsPushNotification> pushManager, final Collection<ExpiredToken> expiredTokens) {
//
//			for (final ExpiredToken expiredToken : expiredTokens) {
//
//				logger.error("Token expired: " + expiredToken);
//				// Stop sending push notifications to each expired token if the
//				// expiration
//				// time is after the last time the app registered that token.
//			}
//		}
//	}
//
//	private class MyRejectedNotificationListener implements RejectedNotificationListener<SimpleApnsPushNotification> {
//
//		@Override
//		public void handleRejectedNotification(final PushManager<? extends SimpleApnsPushNotification> pushManager,
//				final SimpleApnsPushNotification notification, final RejectedNotificationReason reason) {
//
//			logger.warn(notification + " was rejected with rejection reason " + reason);
//			System.out.format("%s was rejected with rejection reason %s\n", notification, reason);
//		}
//	}
//
//	private class MyFailedConnectionListener implements FailedConnectionListener<SimpleApnsPushNotification> {
//
//		@Override
//		public void handleFailedConnection(final PushManager<? extends SimpleApnsPushNotification> pushManager, final Throwable cause) {
//
//			if (cause instanceof SSLHandshakeException) {
//
//				logger.error("SSL handshake exception. Shutting down PushManager..", cause);
//				shutdownPushManager();
//
//			}
//		}
//	}
//
//}