/**
 *
 */
package at.lw.views;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import at.lw.exceptions.PayloadException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.lw.beans.APNManagerSingleton;
import at.lw.misc.RepeatMode;

/**
 * @author Ludwig Werzowa
 */
@ManagedBean
public class IndexManager {

    private static final Logger logger = LoggerFactory.getLogger(IndexManager.class);

    @EJB
    private APNManagerSingleton pushManagerSingleton;

    private RepeatMode repeatMode = RepeatMode.REPEAT_ALL;
    private int repeatTimes = 1;
    private String intensityOne = "250", intensityTwo = "0", intensityThree = "255", intensityFour;
    private String durationOne = "1000", durationTwo = "500", durationThree = "1500", durationFour;

    public IndexManager() {
        logger.debug("Constructor called");
    }

    public void buttonSendNotification() {
        addMessage("Button activated!");

        logger.debug("Repeat mode: " + repeatMode.getCode() + ", Repeats: " + repeatTimes);
        logger.debug("Intensities: " + intensityOne + ", " + intensityTwo + ", " + intensityThree + ", " + intensityFour);
        logger.debug("Durations: " + durationOne + ", " + durationTwo + ", " + durationThree + ", " + durationFour);

        try {
            Map<String, String> values = generatePayloadMap(intensityOne, durationOne, intensityTwo, durationTwo, intensityThree, durationThree, intensityFour, durationFour);

            pushManagerSingleton.sendNotification(repeatMode.getCode(), repeatTimes, values);
        } catch (PayloadException e) {
            addMessage(e.getMessage());
        }
    }

    /**
     * Creates a Map from the given parameters.
     *
     * @param intensityOne
     * @param durationOne
     * @param intensityTwo
     * @param durationTwo
     * @param intensityThree
     * @param durationThree
     * @param intensityFour
     * @param durationFour
     * @return
     * @throws PayloadException
     */
    public static Map<String, String> generatePayloadMap(String intensityOne, String durationOne, String intensityTwo,
                                           String durationTwo, String intensityThree, String durationThree,
                                           String intensityFour, String durationFour) throws PayloadException {

        Map<String, String> values = new HashMap<>();

        intensityOne = StringUtils.trimToEmpty(intensityOne);
        intensityTwo = StringUtils.trimToEmpty(intensityTwo);
        intensityThree = StringUtils.trimToEmpty(intensityThree);
        intensityFour = StringUtils.trimToEmpty(intensityFour);

        durationOne = StringUtils.trimToEmpty(durationOne);
        durationTwo = StringUtils.trimToEmpty(durationTwo);
        durationThree = StringUtils.trimToEmpty(durationThree);
        durationFour = StringUtils.trimToEmpty(durationFour);

        if (StringUtils.isEmpty(intensityOne) || StringUtils.isEmpty(durationOne)) {
            throw new PayloadException("The first intensity and duration both need a value.");
        } else {
            values.put("intensity1", intensityOne);
            values.put("duration1", String.valueOf(durationOne.substring(0, durationOne.length() - 1)));
        } if (intensityTwo != durationTwo && (StringUtils.isEmpty(intensityTwo) || StringUtils.isEmpty(durationTwo))) {
            throw new PayloadException("Intensity and duration either both have to be null or they both need a value.");
        } else if (StringUtils.isNotEmpty(intensityOne) && StringUtils.isNotEmpty(intensityTwo)) {
            values.put("intensity2", intensityTwo);
            values.put("duration2", String.valueOf(durationTwo.substring(0, durationTwo.length() - 1)));
        }
        if (intensityThree != durationThree && (StringUtils.isEmpty(intensityThree) || StringUtils.isEmpty(durationThree))) {
            throw new PayloadException("Intensity and duration either both have to be null or they both need a value.");
        } else if (StringUtils.isNotEmpty(intensityOne) && StringUtils.isNotEmpty(intensityTwo) && StringUtils.isNotEmpty(intensityThree)) {
            values.put("intensity3", intensityThree);
            values.put("duration3", String.valueOf(durationThree.substring(0, durationThree.length() - 1)));
        }
        if (intensityFour != durationFour && (StringUtils.isEmpty(intensityFour) || StringUtils.isEmpty(durationFour))) {
            throw new PayloadException("Intensity and duration either both have to be null or they both need a value.");
        } else if (StringUtils.isNotEmpty(intensityOne) && StringUtils.isNotEmpty(intensityTwo) && StringUtils.isNotEmpty(intensityThree) && StringUtils.isNotEmpty(intensityFour)) {
            values.put("intensity4", intensityFour);
            values.put("duration4", String.valueOf(durationFour.substring(0, durationFour.length() - 1)));
        }
        return values;
    }

    public void addMessage(String summary) {
        logger.warn(summary);
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void buttonSignalOnOff(ActionEvent actionEvent) {

        logger.debug("buttonSignalOnOff activated");

        repeatMode = RepeatMode.REPEAT_ALL;
        repeatTimes = 5;
        intensityOne = "255";
        intensityTwo = "0";
        intensityThree = null;
        intensityFour = null;
        durationOne = "200";
        durationTwo = "150";
        durationThree = null;
        durationFour = null;
    }

    public void buttonSignalRaising(ActionEvent actionEvent) {

        logger.debug("buttonSignalRaising activated");

        repeatMode = RepeatMode.REPEAT_ALL;
        repeatTimes = 3;
        intensityOne = "25";
        intensityTwo = "100";
        intensityThree = "180";
        intensityFour = "255";
        durationOne = "300";
        durationTwo = "300";
        durationThree = "300";
        durationFour = "500";
    }

    public void buttonSignalLowLowHigh(ActionEvent actionEvent) {

        logger.debug("buttonSignalLowLowHigh activated");

        repeatMode = RepeatMode.REPEAT_SECOND;
        repeatTimes = 4;
        intensityOne = "180";
        intensityTwo = "80";
        intensityThree = "255";
        intensityFour = null;
        durationOne = "500";
        durationTwo = "500";
        durationThree = "1200";
        durationFour = null;
    }

    public int getRepeatTimes() {
        return repeatTimes;
    }

    public void setRepeatTimes(int repeatTimes) {
        this.repeatTimes = repeatTimes;
    }

    public String getIntensityOne() {
        return intensityOne;
    }

    public void setIntensityOne(String intensityOne) {
        this.intensityOne = intensityOne;
    }

    public String getIntensityTwo() {
        return intensityTwo;
    }

    public void setIntensityTwo(String intensityTwo) {
        this.intensityTwo = intensityTwo;
    }

    public String getIntensityThree() {
        return intensityThree;
    }

    public void setIntensityThree(String intensityThree) {
        this.intensityThree = intensityThree;
    }

    public String getIntensityFour() {
        return intensityFour;
    }

    public void setIntensityFour(String intensityFour) {
        this.intensityFour = intensityFour;
    }

    public String getDurationOne() {
        return durationOne;
    }

    public void setDurationOne(String durationOne) {
        this.durationOne = durationOne;
    }

    public String getDurationTwo() {
        return durationTwo;
    }

    public void setDurationTwo(String durationTwo) {
        this.durationTwo = durationTwo;
    }

    public String getDurationThree() {
        return durationThree;
    }

    public void setDurationThree(String durationThree) {
        this.durationThree = durationThree;
    }

    public String getDurationFour() {
        return durationFour;
    }

    public void setDurationFour(String durationFour) {
        this.durationFour = durationFour;
    }

    public RepeatMode getRepeatMode() {
        return repeatMode;
    }

    public void setRepeatMode(RepeatMode repeatMode) {
        this.repeatMode = repeatMode;
    }

    public RepeatMode[] getRepeatModes() {
        return RepeatMode.values();
    }

}
