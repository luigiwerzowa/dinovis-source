package at.lw.views;

import at.lw.exceptions.PayloadException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author Ludwig Werzowa
 * @date 14.04.16
 */
public class IndexManagerTest {

    @Test
    public void generatePayloadMapButtonSignalRaising() throws Exception {

        String intensityOne = "25";
        String intensityTwo = "100";
        String intensityThree = "180";
        String intensityFour = "255";
        String durationOne = "300";
        String durationTwo = "300";
        String durationThree = "300";
        String durationFour = "500";

        Map<String, String> values = IndexManager.generatePayloadMap(intensityOne, durationOne, intensityTwo, durationTwo, intensityThree, durationThree, intensityFour, durationFour);

        assertEquals("8 entries expected", 8, values.size());
        assertEquals("first intensity should be 25", "25", values.get("intensity1"));
        assertEquals("second intensity should be 100", "100", values.get("intensity2"));
        assertEquals("third intensity should be 180", "180", values.get("intensity3"));
        assertEquals("forth intensity should be 255", "255", values.get("intensity4"));

        assertEquals("first duration should be 30", "30", values.get("duration1"));
        assertEquals("second duration should be 30", "30", values.get("duration2"));
        assertEquals("third duration should be 30", "30", values.get("duration3"));
        assertEquals("forth duration should be 50", "50", values.get("duration4"));

    }

    @Test
    public void generatePayloadMapButtonSignalLowLowHigh() throws Exception {

        String intensityOne = "180";
        String intensityTwo = "80";
        String intensityThree = "255";
        String intensityFour = null;
        String durationOne = "500";
        String durationTwo = "500";
        String durationThree = "1200";
        String durationFour = null;

        Map<String, String> values = IndexManager.generatePayloadMap(intensityOne, durationOne, intensityTwo, durationTwo, intensityThree, durationThree, intensityFour, durationFour);

        assertEquals("6 entries expected", 6, values.size());
        assertEquals("first intensity should be 180", "180", values.get("intensity1"));
        assertEquals("second intensity should be 80", "80", values.get("intensity2"));
        assertEquals("third intensity should be 255", "255", values.get("intensity3"));

        assertEquals("first duration should be 50", "50", values.get("duration1"));
        assertEquals("second duration should be 50", "50", values.get("duration2"));
        assertEquals("third duration should be 120", "120", values.get("duration3"));

    }

    @Test(expected = PayloadException.class)
    public void generatePayloadMapInvalidEmptyField() throws Exception {

        String intensityOne = "180";
        String intensityTwo = ""; // empty, should throw an exception
        String intensityThree = "255";
        String intensityFour = null;
        String durationOne = "500";
        String durationTwo = "500";
        String durationThree = "1200";
        String durationFour = null;

        IndexManager.generatePayloadMap(intensityOne, durationOne, intensityTwo, durationTwo, intensityThree, durationThree, intensityFour, durationFour);

    }

    @Test(expected = PayloadException.class)
    public void generatePayloadMapInvalidNullField() throws Exception {

        String intensityOne = "180";
        String intensityTwo = "80";
        String intensityThree = "255";
        String intensityFour = null;
        String durationOne = "500";
        String durationTwo = null; // null, should throw an exception
        String durationThree = "1200";
        String durationFour = null;

        IndexManager.generatePayloadMap(intensityOne, durationOne, intensityTwo, durationTwo, intensityThree, durationThree, intensityFour, durationFour);

    }

    @Test(expected = PayloadException.class)
    public void generatePayloadMapInvalidSpacesField() throws Exception {

        String intensityOne = "180";
        String intensityTwo = "80";
        String intensityThree = "255";
        String intensityFour = null;
        String durationOne = "500";
        String durationTwo = "      "; // two or more spaces should count as empty and in THIS case throw an exception
        String durationThree = "1200";
        String durationFour = null;

        IndexManager.generatePayloadMap(intensityOne, durationOne, intensityTwo, durationTwo, intensityThree, durationThree, intensityFour, durationFour);
    }

    @Test
    public void generatePayloadMapButtonSignalRaisingWithSpaces() throws Exception {

        // spaces should be removed
        String intensityOne = "     25";
        String intensityTwo = " 100";
        String intensityThree = " 180     ";
        String intensityFour = "255";
        String durationOne = " 300   ";
        String durationTwo = "300 ";
        String durationThree = " 300";
        String durationFour = "  500           ";

        Map<String, String> values = IndexManager.generatePayloadMap(intensityOne, durationOne, intensityTwo, durationTwo, intensityThree, durationThree, intensityFour, durationFour);

        assertEquals("8 entries expected", 8, values.size());
        assertEquals("first intensity should be 25", "25", values.get("intensity1"));
        assertEquals("second intensity should be 100", "100", values.get("intensity2"));
        assertEquals("third intensity should be 180", "180", values.get("intensity3"));
        assertEquals("forth intensity should be 255", "255", values.get("intensity4"));

        assertEquals("first duration should be 30", "30", values.get("duration1"));
        assertEquals("second duration should be 30", "30", values.get("duration2"));
        assertEquals("third duration should be 30", "30", values.get("duration3"));
        assertEquals("forth duration should be 50", "50", values.get("duration4"));

    }

    @Test
    public void stringUtilsTest() throws Exception {
        assertEquals("12", StringUtils.trimToEmpty("    12 "));
        assertEquals("12", StringUtils.stripToEmpty("    12 "));

        assertEquals("", StringUtils.trimToEmpty("  "));
        assertEquals("", StringUtils.stripToEmpty("  "));

        assertTrue(StringUtils.isEmpty(""));
        assertTrue(StringUtils.isEmpty(null));
        assertTrue(StringUtils.isBlank(" "));
        assertTrue(StringUtils.isBlank("      "));
        assertTrue(StringUtils.isNoneBlank("    12  "));
    }

}